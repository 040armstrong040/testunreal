// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestUGameMode.h"
#include "TestUHUD.h"
#include "TestUCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATestUGameMode::ATestUGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATestUHUD::StaticClass();
}
