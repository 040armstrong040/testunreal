// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestUGameMode.generated.h"

UCLASS(minimalapi)
class ATestUGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestUGameMode();
};



